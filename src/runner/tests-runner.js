var assert = require('assert');
var Q = require('q');
var os = require('os');
var argv = require('minimist')(process.argv.slice(2));
var Verror = require('verror');
var uuid = require('uuid');

var trackingScript = 'https://s3-us-west-1.amazonaws.com/test-bot-tracker/test-bot-tracker.local.js';

validateArgs(argv);

var scenariosService = require('../scenarios-service');
var mutationTracking = require('../mutation-tracking');
var utils = require('../utils');

var webdriver = require('selenium-webdriver');
var chrome = require('selenium-webdriver/chrome');

var opts = new chrome.Options();
opts.addArguments(['user-agent=Usabilitics']);

var driver = new webdriver.Builder()
    //.forBrowser('firefox')
    .usingServer(argv.webdriverServerUrl)
    .withCapabilities(opts.toCapabilities())
    .build();

driver.manage().window().maximize();

var accessToken = process.env.USABILITICS_ACCESS_TOKEN;

var service = scenariosService({
    apiServerUrl: argv.apiUrl,
    appId: argv.appId,
    accessToken: accessToken
});

var runId = global.usabiliticsRunId;

Q.all([service.getScenarios(), service.getTestData(), service.newTestRun(runId)])
    .then(function (results) {
        var scenarios = results[0];
        var testData = results[1];

        describe('Automatically generated UI tests by Usabilitics', function() {
            createTests(scenarios, testData, runId);
        });

        run();
    })
    .catch(function (err) {
        console.log(err.toString());
    });

function createTests (scenarios, testData, runId) {
    if (scenarios.length === 0) {
        console.log('No scenarios available.');
    }

    scenarios.forEach(function(scenario, index) {
        var name = scenario.name ? 'should' + scenario.name : 'scenario ' + index;
        var scenarioId = scenario.scenarioId;

        if (scenario.ignored) {
            xit(name, function () {});
            return;
        }

        it(name, function(done) {
            this.test.scenarioId = scenarioId;
            this.test.started = new Date();
            var errors = [];

            scenario.steps.reduce(function (lastStepPromice, step) {
                return lastStepPromice.then(function (lastStepResult) {
                    if (step.activityType === 'Event') {
                        return Q.fcall(function () {
                                var stepData = findStepData(step, testData);
                                return executeEvent(step, stepData);
                            })
                            .then(function () {
                                return Q(assertEventMutations(step, scenarioId, runId))
                                    .catch(function (err) {
                                        // We collect all errors about unexpected changes in the html mutations and throw them in the end of the scenario.
                                        // This is because we want to see and verify all such errors in one run, instead of stoping the test after the first error.
                                        // If the error is related to the control flow, it will be thrown.
                                        console.log('Error: ' + err.message || err);
                                        errors.push({
                                            step: 'Step: ' + step.type + ' ' + step.targetSelector,
                                            error:err
                                        });
                                        return errors;
                                    });
                            }, function (error) {
                                var err = new Verror(error, 'Error when [' + step.type + '] ['+ step.targetSelector + '].');
                                service.saveErrorOnExecutedStep(scenarioId, runId, step, err.message);
                                throw err;
                            });
                    } else if (step.activityType === 'Mutation') {
                        return Q.fcall(function () {
                            return processMutation(step);
                        });
                    } 
                });
            }, Q())
            .then(function () {
                if (errors.length > 0) {
                    throw new Verror(errors.map(function (e) {
                        return e.step + '\n    ' + e.error.message.split('\n').join('\n    ') ||
                            e.error.split('\n').join('\n    ');
                    }).join('\n'),
                         errors.length > 1 ?
                             'Several errors occured when validating the mutated html.':
                             'Error occured when validating the mutated html.');
                }

                done();
            })
            .done();
        });
    });

    after(function (done) {
        driver.quit().then(done);
    });
 }

 function executeEvent (step, stepData) {
     if (stepData && stepData.requiresData && !stepData.testData) {
         throw new Verror('Test data is missing. This step requires test data.');
     }

     var promise;
     if (step.type !== 'Visited') {
         promise = ensurePageIsOpened(step);
     }

     return Q.when(promise)
         .then(function () {
             console.log('Step: ' + step.type + ' ' + step.targetSelector);
             if (step.type === 'Visited') {
                 return visitPage(step.targetSelector);
             }
             if (step.type === 'Click') {
                 return click(step);
             }
             if (step.type === 'EnterPressed') {
                 return enterPressed(step, stepData);
             }
             if (step.type === 'Focus') {
                 return focus(step, stepData);
             }
         });
 }

 function ensurePageIsOpened(step) {
     return driver.getCurrentUrl()
        .then(function (url) {
            url = utils.updateQueryString('usabilitics', null, url);
            if (url !== step.pageUrl) {
                return visitPage(step.pageUrl);
            }
        });
 }

 function ensureTrackingScriptIsLoaded(appId, scriptUrl) {
    return Q(driver.executeScript(mutationTracking.checkForTrackingScript)
        .then(function (isLoaded) {
            if (isLoaded) {
                return true;
            } else {
                return Q(driver.executeScript(mutationTracking.loadTrackingScript, appId, scriptUrl));
            }
        }));
 }

 function visitPage (stepUrl) {
     var url = utils.updateQueryString('usabilitics', 'true', stepUrl);
     return driver.get(url)
         .then(function () {
             return ensureTrackingScriptIsLoaded(argv.appId, trackingScript);
         })
         .then(disableAnimations)
         .then(function () {
             return driver.controlFlow().timeout(2000);
         });
 }

 function click (step) {
     var selector = step.targetSelector || 'body';
     var element = driver.findElement(webdriver.By.css(selector));
     return element.isDisplayed()
         .then(function (val) {
             if (val) {
                 return driver.wait(webdriver.until.elementIsEnabled(element), 2000)
                     .then(function () {
                         return element.click();
                     });
             } else {
                 return tryHoverAndClick(element);
             }
         });
 }

 function enterPressed (step, stepData) {
     var element = driver.findElement(webdriver.By.css(step.targetSelector));
     return Q(driver.wait(webdriver.until.elementIsVisible(element), 2000))
         .then(function () {
             if (stepData) {
                 return element.sendKeys(stepData.testData);
             }
         }).then(function () {
             return element.sendKeys(webdriver.Key.ENTER);
         });
 }

 function focus(step, stepData) {
     var element = driver.findElement(webdriver.By.css(step.targetSelector));
     return Q(driver.wait(webdriver.until.elementIsVisible(element), 2000))
         .then(function () {
             return element.sendKeys('');
         })
         .then(function () {
             if (stepData) {
                 return element.sendKeys(stepData.testData);
             }
         });
 }

 function tryHoverAndClick (element) {
     return driver.executeScript(function () {
             var element = arguments[0];
             element.style.display='block';
         }, element)
     .then(function () {
         return new webdriver.ActionSequence(driver)
             .mouseMove(element)
             .perform();
     })
     .then(function () {
         return driver.executeScript(function () {
             var element = arguments[0];
             element.style.display='';
         }, element);
     })
     .then(function () {
         return element.isDisplayed();
     })
     .then(function (val) {
         if (val) {
             return driver.wait(webdriver.until.elementIsEnabled(element), 2000);
         } else {
             throw new Error('Element not visible and impossible to click.');
         }
     })
     .then(function () {
         return element.click();
     });
 }

 function processMutation (step) {
     console.log('Expecting: ' + step.mutatedElementSelector + ' to be ' + step.type);
     return driver.executeScript(mutationTracking.getNextMutation)
         .then(function (mutation) {
             // if (!mutation) {
             //     throw new Verror('The element %s is not %s', step.mutatedElementSelector, step.type);
             // } else if (mutation.mutatedElementSelector === step.mutatedElementSelector &&
             //          mutation.type === step.type) {
             // } else {
             //     throw new Verror('The element %s is not %s. Got %s which is %s instead.',
             //         step.mutatedElementSelector,
             //         step.type,
             //         mutation.mutatedElementSelector,
             //         mutation.type);
             // }
         }, function (err) {
             throw new Verror(err,
                 'Executing javascript in the browser failed. Trying to get mutation - The element %s is %s',
                 step.mutatedElementSelector,
                 step.type);
         });
 }

 function assertEventMutations(step, scenarioId, runId) {
     return driver.executeScript(mutationTracking.getAllMutations)
         .then(function (mutations) {
             return service.assertEventMutations(scenarioId, runId, step, mutations)
         }, function (err) {
             var error = new Verror(err,
                 'Executing javascript in the browser failed. Trying to get mutations.');
             service.saveErrorOnExecutedStep(scenarioId, runId, step, error.message);
             throw error;
         })
         .then(function (result) {
             if (result.assertStatus === 'passed') {

             } else if (result.assertStatus === 'failed') {
                 throw new Verror(result.errors.join('\n'),
                     'Found difference in the mutations caused by the event.');
             }
         }, function (error) {
             throw new Verror(error, 'The server failed when asserting mutations.');
         });
 }

 function findStepData(step, testData) {
     var entryData = testData.filter(function (entry) {
         return entry.nodeId === step.nodeId;
     });

     if (entryData.length === 0) {
         return null;
     } else {
         return entryData[0];
     }
 }

function disableAnimations() {
    return Q(driver.executeScript(
            "if(typeof $ !== 'undefined') $.fx ? $.fx.off = true : '';" +
            "var styleEl = document.createElement('style');" +
            "styleEl.textContent = '*{ transition: none !important; transition-property: none !important; transform: none !important; animation: none !important; }';" +
            "document.head.appendChild(styleEl);"
        ));
}

 function validateArgs (argv) {
     if (!argv.webdriverServerUrl) {
         throw 'Invalid argument: ' + 'webdriverServerUrl is not presented.';
     }

     if (!argv.apiUrl) {
         throw 'Invalid argument: ' + 'apiUrl is not presented.';
     }

     if (!argv.appId) {
         throw 'Invalid argument: ' + 'appId is not presented.';
     }
 }