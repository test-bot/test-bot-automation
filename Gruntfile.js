module.exports = function(grunt) {

    // Project configuration.
    grunt.initConfig({
      pkg: grunt.file.readJSON('package.json'),
  
      jshint: {
            // define the files to lint
            files: ['gruntfile.js',
                'bin/*.js',
                'src/**/*.js',
                'src/*.js'
            ],
            // configure JSHint (documented at http://www.jshint.com/docs/)
            options: {
                // more options here if you want to override JSHint defaults
                globals: {},
                laxcomma: true
            }
        },

        watch: {
            js: {
                files: ['bin/*.js',
                        'src/**/*.js',
                        'src/*.js'],
                tasks: ['jshint'],
                options: {
                    livereload: true
                }
            }
        },

      mochaTest: {
          unit: {
              src: ['tests/*.js'],
              options: {
                  reporter: 'spec'
              }
          }
      }

    });
  
    // Load the plugins
    grunt.loadNpmTasks('grunt-mocha-test');
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-watch');
  
    // Default task(s).
    grunt.registerTask('default', ['mochaTest']);

    grunt.registerTask('dev', ['watch:js']);
};