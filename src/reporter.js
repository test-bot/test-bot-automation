module.exports = function (runner, apiUrl, appId, runId) {
    var accessToken = process.env.USABILITICS_ACCESS_TOKEN;

    var scenariosService = require('../src/scenarios-service');

    var service = scenariosService({
        apiServerUrl: apiUrl,
        appId: appId,
        accessToken: accessToken
    });

    var runStats = {
        passed: 0,
        failed: 0,
        scenarios: []
    };

    runner.on('pass', function (test) {
        runStats.passed++;
        runStats.scenarios.push({
            scenarioId: test.scenarioId,
            title: test.title,
            status: 'passed',
            started: test.started,
            ended: new Date()
        });
    });
    runner.on('fail', function (test, err) {
        runStats.failed++;
        runStats.scenarios.push({
            scenarioId: test.scenarioId,
            title: test.title,
            status: 'failed',
            message: err.message,
            started: test.started,
            ended: new Date()
        });
    });
    runner.on('end', function () {
        runStats.ended = new Date();
        service.updateTestRun(runId, runStats);
    });
}