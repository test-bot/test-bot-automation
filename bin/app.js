#! /usr/bin/env node

var argv = require('minimist')(process.argv.slice(2));

var uuid = require('uuid');

var Mocha = require('mocha'),
    fs = require('fs'),
    path = require('path');

// Instantiate a Mocha instance.
var mocha = new Mocha({
    enableTimeouts: false
});

mocha.delay();

var testDir = 'src/runner';

// Add each .js file to the mocha instance
fs.readdirSync(testDir).filter(function(file){
    // Only keep the .js files
    return file.substr(-3) === '.js';

}).forEach(function(file){
    mocha.addFile(
        path.join(testDir, file)
    );
});

var runId = uuid.v1();
global.usabiliticsRunId = runId;

// Run the tests.
var runner = mocha.run(function(failures){
  process.on('exit', function () {
    process.exit(failures);  // exit with non-zero status if there were failures
  });
});

var reporter = require('../src/reporter');
reporter(runner, argv.apiUrl, argv.appId, runId);