// This method will be executed on the client by Web driver
module.exports = {

    getNextMutation: function () {
        if(!window.usabiliticsSettings)
            throw new Error('There is no tracking script on the page.');

        var data = window.usabiliticsSettings.backend.collectedData;
        while(data[0] && data[0].mutations.length === 0) {
            data.shift();
        }
        var mutation = data[0].mutations.shift();
        return mutation;
    },

    getAllMutations: function () {
        if(!window.usabiliticsSettings)
            throw new Error('There is no tracking script on the page.');

        var data = window.usabiliticsSettings.backend.collectedData;

        if (!data) {
            window.usabiliticsSettings.backend.collectedData = [];
            return [];
        }

        var mutations = data.filter(function (dataRecord) {
                return dataRecord.mutations.length > 0;
            })
            .reduce(function (prev, current) {
                Array.prototype.push.apply(prev, current.mutations);
                return prev;
            }, []);

        window.usabiliticsSettings.backend.collectedData = [];
        return mutations;
    },

    checkForTrackingScript: function () {
        return !!window.usabiliticsSettings;
    },

    loadTrackingScript: function (appId, scriptUrl) {
        window.usabiliticsSettings = {
            appId: appId,
            visitedTimestamp: Date.now()
        };

        var s = document.createElement('script');
        s.type = 'text/javascript';
        s.async = true;
        s.src = scriptUrl;
        document.getElementsByTagName('head')[0].appendChild(s);
    }
};