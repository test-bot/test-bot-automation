module.exports = function (options) {
    var request = require('request');
    var Q = require('q');
    var os = require('os');

    process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0"

    var machineName = os.hostname();

    function getScenarios () {
        var deferred = Q.defer();

        var url = options.apiServerUrl + '/v1/apps/' + options.appId + '/scenarios';
        var params = {
            url: url,
            method: 'GET',
            rejectUnhauthorized : false,
            auth: {
                'bearer': options.accessToken
            } 
        };

        request(params, function (error, response, body) {
            if (!error && response.statusCode == 200) {
                deferred.resolve(JSON.parse(body).items);
            }
            else {
                deferred.reject(error);
            }
        });
        return deferred.promise;
    }

    function assertEventMutations(scenarioId, runId, step, mutations) {
        var deferred = Q.defer();

        var url = options.apiServerUrl + '/v1/apps/' + options.appId + '/scenarios/' + scenarioId + '/assert/step';
        var params = {
            url: url,
            method: 'POST',
            json: true,
            rejectUnhauthorized : false,
            body: {
                nodeId: step.nodeId,
                stepId: step.stepId,
                runId: runId,
                mutations: mutations,
                targetSelector: step.targetSelector,
                type: step.type
            },
            auth: {
                'bearer': options.accessToken
            }
        };

        request(params, function (error, response, body) {
            if (!error && response.statusCode == 200) {
                deferred.resolve(body);
            }
            else {
                deferred.reject(error);
            }
        });
        return deferred.promise;
    }

    function saveErrorOnExecutedStep(scenarioId, runId, step, error) {
        var deferred = Q.defer();

        var url = options.apiServerUrl + '/v1/apps/' + options.appId + '/scenarios/' + scenarioId + '/error';
        var params = {
            url: url,
            method: 'POST',
            json: true,
            rejectUnhauthorized : false,
            body: {
                nodeId: step.nodeId,
                stepId: step.stepId,
                runId: runId,
                error: error,
                targetSelector: step.targetSelector,
                type: step.type
            },
            auth: {
                'bearer': options.accessToken
            }
        };

        request(params, function (error, response, body) {
            if (!error && response.statusCode == 200) {
                deferred.resolve(body);
            }
            else {
                deferred.reject(error);
            }
        });
        return deferred.promise;
    }

    function getTestData(argument) {
        var deferred = Q.defer();

        var url = options.apiServerUrl + '/v1/apps/' + options.appId + '/test-data';
        var params = {
            url: url,
            method: 'GET',
            rejectUnhauthorized : false,
            auth: {
                'bearer': options.accessToken
            }
        };

        request(params, function (error, response, body) {
            if (!error && response.statusCode == 200) {
                deferred.resolve(JSON.parse(body).items);
            }
            else {
                deferred.reject(error);
            }
        });
        return deferred.promise;
    }

    function newTestRun(runId) {
        var deferred = Q.defer();

        var url = options.apiServerUrl + '/v1/apps/' + options.appId + '/test-runs';
        var params = {
            url: url,
            method: 'POST',
            json: true,
            rejectUnhauthorized : false,
            body: {
                appId: options.appId,
                runId: runId,
                machineName: machineName,
                started: new Date()
            },
            auth: {
                'bearer': options.accessToken
            }
        };

        request(params, function (error, response, body) {
            if (!error && response.statusCode == 200) {
                deferred.resolve();
            }
            else {
                deferred.reject(error);
            }
        });
        return deferred.promise;
    }

    function updateTestRun(runId, stats) {
        var deferred = Q.defer();

        stats.scenarios.forEach(function (s) {
            s.machineName = machineName;
        });

        var url = options.apiServerUrl + '/v1/apps/' + options.appId + '/test-runs/' + runId;
        var params = {
            url: url,
            method: 'PUT',
            json: true,
            rejectUnhauthorized : false,
            body: {
                ended: stats.ended,
                failed: stats.failed,
                passed: stats.passed,
                scenarios: stats.scenarios
            },
            auth: {
                'bearer': options.accessToken
            }
        };

        request(params, function (error, response, body) {
            if (!error && response.statusCode == 200) {
                deferred.resolve();
            }
            else {
                deferred.reject(error);
            }
        });
        return deferred.promise;
    }

    return {
        getScenarios: getScenarios,
        assertEventMutations: assertEventMutations,
        saveErrorOnExecutedStep: saveErrorOnExecutedStep,
        getTestData: getTestData,
        newTestRun: newTestRun,
        updateTestRun: updateTestRun
    };
};